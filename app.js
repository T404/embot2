var app, express, io, server, ws, mongojs, nodemailer, fs, path, request, http, cheerio;

var emails = [];
var globalUrl = '';
var currentLink = 61;
var pageReadyNum = 0;
var globalFile;
var j = 1;
var gSocket;

var gFirst = 61;
var gLast = 121;

express = require('express');
app = express();
ws = require('socket.io');
mongojs = require('mongojs');
nodemailer = require("nodemailer");
fs = require('fs');
path = require('path');
http = require('http');
request = require('request');
cheerio = require("cheerio");

// create reusable transport method (opens pool of SMTP connections)
var smtpTransport = nodemailer.createTransport({
	service: 'Yahoo',
	auth: {
		user: 'mail.pickapp@yahoo.com',
		pass: 'Smoove71'
	}
});

// Connecting mongoDB
/*var db = mongojs('adme:Smoove7@ds039020.mongolab.com:39020/recall');
var user = db.collection('users');*/

app.use(express["static"]('./static'));

// Renders main
app.get('/', function(req, res) {

	if (req.headers.host.match(/^www/) !== null ) {
	
		res.redirect('http://' + req.headers.host.replace(/^www\./, '') + req.url);
	
	} else { res.render('index'); };
});

// Set server
var port = process.env.OPENSHIFT_NODEJS_PORT || 8080;
var ip = process.env.OPENSHIFT_NODEJS_IP  || 0;

server = app.listen(port, ip);
mess('Server running at ' + port);
io = ws.listen(server);

// Only websockets, no logging
io.set('transports', ['websocket']);
mess('Websockets waiting...');
io.set('log level', 1);

// On Conn
io.sockets.on('connection', function(socket) {

	mess('Connection live!');

	gSocket = socket;

	socket.on('start', function(msg) {

		if (msg.pass == 'done') {

			scrapeEmails('urls.txt');
		};
	});

	socket.on('disconnect', function() {

		mess('Disconnected');
	});
});

//scrapeGoogle('https://www.google.hu/search?q=info+apro', 17);

function scrapeGoogle(url, pages) {

	mess('Crawling google...');

	var j = 1;
	var x = 1;
	var urls = [];

	// normally from 0 to pages
	for (var i = 18; i < 22; i++) {

		url = url + '&start=' + i*10;

		request({uri: url}, function(error, response, html) {

			mess(x + ': ' + response.statusCode);
			x++;

			if (!error && response.statusCode == 200) {
			
				var site = cheerio.load(html);

				site('h3.r a').each(function(i) {
		
					var link = cheerio(this);
					var newLink = getLink(link.attr('href'));

					newLink = newLink.substr(7, newLink.length);

					if (newLink.length > 8) {

						urls[j] = newLink;

						fs.appendFile('urls.txt', newLink + '\r\n', function (err) {

							if (err) return mess(err);
							mess('added to txt: ' + newLink);
						});

						mess('Url found: ' + j + ' - ' + newLink);
						j++;
					};
				});
			};
		});
	};
};

function scrapeEmails(txt) {

	var links = [];
	var x = 1;

	var fs = require('fs');
	fs.readFile( __dirname + '/urls.txt', function (err, data) {
		
		if (err) return mess(err);

		links = data.toString().split('\r\n');
		globalFile = links;
		mess(txt + ' loaded!');

		globalUrl = links[gFirst].substr(11, links[gFirst].length);
		globalUrl = globalUrl.substr(0, globalUrl.length-6).toString();

		links[gFirst] = links[gFirst] + 'apro/';
		
		for (var o = 1; o < 16; o++) {
		
			crawl(links[gFirst].toString() + o, globalUrl, o);
		};

		emails[globalUrl] = [];
		j = 1;
		mess('Crawling from ' + globalUrl);
	});
};

function crawl(url, gUrl, currentPage) {

	var headers = {
		'User-Agent': 'Super Agent/0.0.1',
		'Content-Type': 'application/x-www-form-urlencoded'
	};

	var options = {

		uri: url,
		headers: headers
	};

	var emLength = 0;
	var currentUrl = url.substr(0, url.length-2);

	var nextPage = currentPage + 1;
	var nextUrl;

	request(options, function(error, response, html) {

		if (!error && response.statusCode == 200) {
		
			var page = cheerio.load(html);
			var pageLength = page('script').length;

			if (pageLength == 0) pageLength = 1;

			page('script').each(function(i) {

				emLength++;
	
				var link = page(this).toString();

				var email = link.substr(99, link.length).toString();
				email = email.substr(0, email.length-19);
				email = email.replace(/\"/g, '');
				email = email.replace(/\+/g, '');
				email = email.replace(/\ /g, '');

				var repeat = false;

				if (emails[globalUrl][0]) {

					for (var k = 0; k < emails[globalUrl].length; k++) {

						if (email == emails[globalUrl][k]) repeat = true;
						
						if (k == emails[globalUrl].length-1 && !repeat) {

							if (email.length > 4 && validEmail(email)) {
								
								emails[globalUrl][j] = email;
								j++;
							};
						};
					};

				} else {

					if (email.length > 4 && validEmail(email)) {
					
						emails[globalUrl][0] = email;
						j++;
					};
				};

				if (emLength == pageLength) {

					pageReadyNum++;

					if (pageReadyNum < 15) {

						gSocket.emit('page', {page: currentPage});

					} else if (pageReadyNum == 15) {

						pageReadyNum = 0;
						gSocket.emit('page', {page: currentPage});

						if (currentLink < gLast) {

							if (emails[globalUrl].length > 0) {

								for (var k = 0; k < emails[globalUrl].length; k++) {

									if (emails[globalUrl][k] && emails[globalUrl][k] != 'undefined') {
		
										fs.appendFile('static/emails/' + gUrl + '.txt', emails[globalUrl][k] + '\r\n', function (err) {

											if (err) return mess(err);
										});
									};

									if (k == emails[globalUrl].length-1) {

										mess('added to txt: ' + gUrl + '.txt');

										gSocket.emit('site', {site: gUrl, current: currentLink, num: emails[globalUrl].length});

										currentLink++;

										if (currentLink < gLast) {
								
											globalUrl = globalFile[currentLink].substr(11, globalFile[currentLink].length);
											globalUrl = globalUrl.substr(0, globalUrl.length-6).toString();
											nextUrl = globalFile[currentLink] + 'apro/';

											mess('Crawling from ' + globalUrl);
											emails[globalUrl] = [];
											j = 1;

											for (var o = 1; o < 16; o++) {
				
												crawl(nextUrl.toString() + o, globalUrl, 1);
											};

										} else if (currentLink == gLast) {

											gSocket.emit('done', '');
										};
									}
								};

							} else {

								gSocket.emit('site', {site: gUrl, current: currentLink, num: emails[globalUrl].length});

								currentLink++;

								globalUrl = globalFile[currentLink].substr(11, globalFile[currentLink].length);
								globalUrl = globalUrl.substr(0, globalUrl.length-6).toString();
								nextUrl = globalFile[currentLink] + 'apro/';

								mess('Crawling from ' + globalUrl);
								emails[globalUrl] = [];
								j = 1;

								for (var o = 1; o < 16; o++) {

									crawl(nextUrl.toString() + o, globalUrl, 1);
								};
							};

						} else if (currentLink == gLast) {

							gSocket.emit('done', '');
						};
					};
				};
			});

		} else {

			mess(url + ': ' + error);

			gSocket.emit('site', {site: gUrl, current: currentLink, num: emails[globalUrl].length});

			currentLink++;

			globalUrl = globalFile[currentLink].substr(11, globalFile[currentLink].length);
			globalUrl = globalUrl.substr(0, globalUrl.length-6).toString();
			nextUrl = globalFile[currentLink] + 'apro/';

			mess('Crawling from ' + globalUrl);
			emails[globalUrl] = [];
			j = 1;

			for (var o = 1; o < 16; o++) {

				crawl(nextUrl.toString() + o, globalUrl, 1);
			};
		};
	});
};

function getLink(url) {

	var baseURL = url.substring(0, url.indexOf('/', 14));


	if (baseURL.indexOf('http://localhost') != -1) {

		// Base Url for localhost
		var url = location.href;  // window.location.href;
		var pathname = location.pathname;  // window.location.pathname;
		var index1 = url.indexOf(pathname);
		var index2 = url.indexOf("/", index1 + 1);
		var baseLocalUrl = url.substr(0, index2);

		return baseLocalUrl + "/";
	
	} else {
		
		// Root Url for domain name
		return baseURL + "/";
	}
};

function validEmail(email) {

	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
};

function containsDash(str) {

	var a = /\//g;
	return a.test(str);
};

function mess(msg) {

	var d = new Date(),

	minutes = d.getMinutes().toString().length == 1 ? '0'+d.getMinutes() : d.getMinutes(),
	hours = d.getHours().toString().length == 1 ? '0'+d.getHours() : d.getHours(),
	ampm = d.getHours() >= 12 ? 'pm' : 'am';

	console.log('\n' + hours + ':' + minutes + ampm + ': ' + msg);
};