
var socket = io.connect('ws://bot2-bannedlab.rhcloud.com:8000/');
var allWidth = 0;
var allEmails = 0;
var newWidth = 0;

$('#start').click(function() {

	var pass = prompt("Enter password!", "");

	if (pass != null) {
		socket.emit('start', {pass: pass});
		start = new Date().getTime();
		$('#start').attr('disabled', 'true');
		$('#start').html('working...');
	};
});

socket.on('page', function(message) {

	newWidth += 30;
	$('#log').css({width: newWidth});

	var stat = Math.floor(newWidth/4.5);
	$('#log-stat').html(stat + '%');
});

socket.on('site', function(message) {
	
	newWidth = 0;
	$('#log').css({width: 0});

	allWidth += 7.5;
	$('#log2').css({width: allWidth});

	allEmails += message.num;
	$('#log3').append(message.current + ' ' + message.site + '.info > txt: ' + message.num + '<br>');

	$('#log3').scrollTop(1000000);

	$('#log-stat').html('0');

	var stat = Math.floor(allWidth/4.5);
	$('#log2-stat').html(stat + '%');
});

socket.on('done', function(message) {

	end = new Date().getTime();
	time = end - start;
	time = Math.floor(time/1000);

	$('#log3').append('READY! ' + allEmails + '/' + time + ' sec<br>');

	$('#log3').scrollTop(1000000);

	$('#start').html('done');
});